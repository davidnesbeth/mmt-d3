d3.json("stock.json", function(error, json) {
  if (error) throw error;
  
  // functions for selecting the svg element abd appending a "g" element
  //variables corresponding the the svg height, width and margins
  var svg = d3.select("svg"),
    margin = {top: 20, right: 20, bottom: 30, left: 30},
    width = $("#chart").width(),
    height = +svg.attr("height"),
    g = svg.append("g");

  // used to create d3 time using string in the format "hour:minute:seconds.microseconds"
  var parseTime = d3.timeParse("%H:%M:%S.%L");
  
  // used for scales for x and y axes  
  var x = d3.scaleTime().range([0, width]),
      y = d3.scaleLinear().range([height, 0]);

  // d3 function used to create area above stock information
  var topArea = d3.area()
    .x(function(d) { return x(parseTime(d.timeStr)); })
    .y1(function(d) { return y(toDollarCent(d.ask)); })
    .curve(d3.curveStepAfter);

  // d3 function used to create area with stock information
  var midArea = d3.area()
    .x(function(d) { return x(parseTime(d.timeStr)); })
    .y0(function(d) { return y(toDollarCent(d.ask)); })
    .y1(function(d) { return y(toDollarCent(d.bid)); })
    .curve(d3.curveStepAfter);

  // d3 function used to create area below stock information
  var bottomArea = d3.area()
    .x(function(d) { return x(parseTime(d.timeStr)); })
    .y0(function(d) { return y(toDollarCent(d.bid)); })
    .y1(height)
    .curve(d3.curveStepAfter);

  var bboList = json.bboList; // isolates bboList array in JSON file
  var tradeList = json.tradeList; // isolates tradeList array in JSON file

  x.domain(d3.extent(bboList, function(d) { return parseTime(d.timeStr); })); // sets x-axis domain to min and max timeStr in bboList array
  y.domain([d3.min(bboList, function(d) { return toDollarCent(d.bid); }), d3.max(bboList, function(d) { return toDollarCent(d.ask); })]); // sets y-axis domain to max ask and max bid in bboList array

  // create area above stock information
  g.append("path")
    .attr("class", "area")
    .style("fill", "#3498db")
    .attr("d", topArea(bboList));

  // create area with stock information
  g.append("path")
    .datum(bboList)
    .attr("class", "area")
    .style("fill", "#EEE")
    .attr("d", midArea);

  // create area below stock information
  g.append("path")
    .attr("class", "area")
    .style("fill", "#27ae60")
    .attr("d", bottomArea(bboList));

  // create y-axis label
  svg.append("text")
    .attr("x", -10)
    .attr("y", 45)
    .attr("position", "fixed")
    .style("font", "12.5px arial black")
    .style("text-anchor", "end")
    .attr("transform", "rotate(-90)")
    .text("Price ($)");

  // create reset button used when zooming
  d3.select("body")
    .append("div")
    .attr("id", "resetButton")
    .on("click", resetted)
    .html("RESET");

  // variable used to create tooltip
  var tradeTooltip = d3.select("body")
    .append("div")
    .attr("class", "tradeToolTip");

  // creates circles representing different trades and their sizes... on mouseover trade info is shown
  g.selectAll("circle")
    .data(tradeList)
    .enter().append("circle")
    .attr("class", function(d){  return (d.tradeType == "E") ? "eType" : "pType";  })
    .attr("r", function(d){  return (d.shares/100);  })
    .attr("cx", function(d) {  return x(parseTime(timeToTimeStr(d.time))); })
    .attr("cy", function(d) { return y(toDollarCent(d.price)); })
    .attr("fill", function(d){  return (d.tradeType == "E") ? "#FF0000" : "#000";  })
    .on("mouseover", function(d){
      tradeTooltip.style("visibility", "visible")
      .html("Order Reference #: " + d.orderReferenceNumber + "<br/>Time: " + timeToTimeStr(d.time) + "<br/>Price: $" + toDollarCent(d.price) + "<br/>Shares: " + d.shares);
    })
    .on("mousemove", function(){
      return tradeTooltip.style("top", (event.pageY-10)+"px").style("left",(event.pageX+10)+"px");
    })
    .on("mouseout", function(){
      return tradeTooltip.style("visibility", "hidden");
    });

  // function to add/update x-axis
  var gX =  svg.append("g")
    .attr("class", "axis axis--x")
    .attr("transform", "translate(0," + (height-1) + ")")
    .call(d3.axisTop(x).ticks(8));

  // function to add/update y-axis
  var gY = svg.append("g")
    .attr("class", "axis axis--y")
    .call(d3.axisRight(y));

  // function used to zoom into graph
  var zoom = d3.zoom()
    .scaleExtent([1, 40])
    .translateExtent([[0, 0], [width, height]])
    .on("zoom", zoomed);

  svg.call(zoom);

  // function used to transform graph and its axes upon zoom
  function zoomed() {
    g.attr("transform", d3.event.transform);
    gX.call((d3.axisTop(x).ticks(8)).scale(d3.event.transform.rescaleX(x)));
    gY.call((d3.axisRight(y)).scale(d3.event.transform.rescaleY(y)));
  }

  // function used to reset graph to original position
  function resetted() {
    svg.transition()
      .duration(750)
      .call(zoom.transform, d3.zoomIdentity);
  }

  // asign positioning to reset button based on svg element's position
  $("#resetButton").offset({top: ($("#chart").offset().top + 10)});
  $("#resetButton").offset({left: ($("#chart").offset().left + $("#chart").width() - 85)});

  // update positioning of reset button when window is resized
  $(window).resize(function() {
    $("#resetButton").offset({top: ($("#chart").offset().top + 10)});
    $("#resetButton").offset({left: ($("#chart").offset().left + $("#chart").width() - 85)});
  });

});

// convert time in tradeList to time used format in bboList
function timeToTimeStr(x) {
    x = parseInt(x/1000000);
    x = (new Date(x).toGMTString()).toString().slice(17, 25) + "." + x.toString().slice(-3);
    return x;
  }

// converts hundreds of pennies to dollar value
function toDollarCent(y) {
  return (y/10000);
}

// function used to hide E type trades
function eHide() {
  if ($(".eType").css("display") != "none") {
    $(".eType").hide();
  }
  else {
    $(".eType").show();
  }
}

// function used to hide P type trades
function pHide() {
  if ($(".pType").css("display") != "none") {
    $(".pType").hide();
  }
  else {
    $(".pType").show();
  }
}