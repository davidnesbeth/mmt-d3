var util = require('util');
// setup files
var express  = require('express');
var app      = express();                   // create our app w/ express
var morgan = require('morgan');             // log requests to the console (express4)

app.use(express.static(__dirname + '/public')); // set the static files location /public/img
app.use(morgan('dev'));  // log every request to the console

// listen (start app with node app)
app.listen(process.env.PORT || 8080);
console.log("App listening on port 8080");

// application -------------------------------------------------------------
app.get('*', function(req, res) {
    res.sendfile('./public/index.html'); // load the single view file (angular will handle the page changes on the front-end)
});

